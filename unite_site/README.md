# unite-dev
The basement for a universal language

The idea of having yet another language, when there are thousanda of others, is unoriginal and not innovative at all. But the truth remains that there is a devide between high level languages that make coding easy and low level languages that make faster code. The question then arises, is it possible to create a language, that caters to both these groups? That is precicely the motivation of this language. Language in its essence is the first quality that a human being learns one's they are born into the world, they start by making sounds and noices, then by noticing patterns start to formulate words and thus build on a life long journey of language usage and study. Why cant a computer language, be as natural to normal languages to a human being? This is a driving question behind designing this language. 

in essence: The need for a “learning” computer language 

I am at least familiar with some computer languages like C, python, bash, go and a bit of java. Of all these, I find python the easiest to learn. In fact you can code python in a jiffy. But what if all the faster languages like C or Fortran start to adapt to individual coding styles? What I am hinting here is that you have a very complex code, in the begining, called the seed code and from that as you start coding, the language using machine learning, tries to come up with simplifications based on the individual typing skills. So everyone has a unique language for themselves to code in. And, the algorithm that learns how users use the language individually, prompts the specific user about the change in coding style and the user gets to choose whether they like it or not. This then is implemented throughout, and so the language evolves. The changes should also be able to be made into a log file of some sort so that when using another computer, one can start from scratch, and run this log file to get to their specific coding style. How wonderful that can be! I am too stupid enough to do this on my own, but if people are behind me, i guess we can pull it off. 

16DEC18
The deployment will follow:
  * developement of a sub language called unique which will be supplied with neural networks and learning libraries which will
    promptly learn the user's programming input. So every programmer ends up with a unique representation of the language.
  * The uniques will be then sorted for and learned by the super language, the unite which will act as a dictonary and a standard
    for the general all purpose deployment. This ensures dynamicity and adherance.
  * The unite language will be as close as, if not the same as the majority of the spoken language of humanity.
  * With these in mind, the empowerment of common people is ensured, and the middlemen, us programmers are almost excluded.
